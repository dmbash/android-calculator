package com.gitlab.evilwild.calculator.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@RequiredArgsConstructor
@Accessors(fluent = true)
@Getter
@Setter
public class CalculatorFields {
    private final double a;
    private final double b;
}
