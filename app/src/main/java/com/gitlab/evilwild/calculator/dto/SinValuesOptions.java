package com.gitlab.evilwild.calculator.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Accessors;

@Getter
@Builder
@Accessors(fluent = true)
public class SinValuesOptions {
    private final float centerX;
    private final float centerY;
    @Builder.Default
    private final float stepX = 0.01f;

}
