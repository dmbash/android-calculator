package com.gitlab.evilwild.calculator.dto;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Entity
@Getter
@Builder
@AllArgsConstructor
public class SinValue {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "x")
    private float x;

    @ColumnInfo(name = "y")
    private float y;
}
