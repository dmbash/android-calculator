package com.gitlab.evilwild.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;

import com.gitlab.evilwild.calculator.db.SinValueDatabase;
import com.gitlab.evilwild.calculator.util.GridDrawable;

public class GraphicsActivity extends AppCompatActivity {

    ImageView canvasView;
    private SinValueDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graphics);

        this.db = SinValueDatabase.getInstance(this);

        this.canvasView = this.findViewById(R.id.canvasView);

        GridDrawable grid = new GridDrawable(db);

        this.canvasView.setImageDrawable(grid);
    }
}