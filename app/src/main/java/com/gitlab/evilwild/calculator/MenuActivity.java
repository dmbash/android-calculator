package com.gitlab.evilwild.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.os.Bundle;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    public void onCalculatorClick(View btn) {
        startActivity(new Intent(this, CalculatorActivity.class));
    }

    public void onGraphicsClick(View btn) {
        startActivity(new Intent(this, GraphicsActivity.class));
    }

    public void onExitClick(View btn) {
        finishAffinity();
        System.exit(0);
    }
}