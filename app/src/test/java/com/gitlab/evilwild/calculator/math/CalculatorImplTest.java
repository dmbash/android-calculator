package com.gitlab.evilwild.calculator.math;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CalculatorImplTest {

    private CalculatorImpl calculatorImpl;

    @Before
    public void setUp() throws Exception {
        this.calculatorImpl = new CalculatorImpl();
    }

    @Test
    public void add() {
        assertEquals(0, this.calculatorImpl.add(-5, 5), 0.1);
    }

    @Test
    public void sub() {
        assertEquals(-5, this.calculatorImpl.sub(-10, -5), 0.1);
    }

    @Test
    public void mul() {
        assertEquals(-100, this.calculatorImpl.mul(10, -10), 0.1);
    }

    @Test
    public void pow() {
        assertEquals(3125, this.calculatorImpl.pow(5, 5), 0.1);
    }

    @Test
    public void div() {
        assertEquals(-12.5, this.calculatorImpl.div(25, -2), 0.1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void divisionOnNull() {
        this.calculatorImpl.div(51, 0);
    }

    @Test
    public void percent() {
        assertEquals(20, this.calculatorImpl.percent(20, 100), 0.1);
    }
}